package com.jordansportfolio.samsungremote;

import android.os.AsyncTask;

import com.jordansportfolio.samsungremote.samsungtvcontrol.SamsungRemote;
import com.jordansportfolio.samsungremote.samsungtvcontrol.TVReply;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by Jordan on 1/3/2018.
 */

public class RemoteAsync extends AsyncTask<String, Void, String> {

    private final String ipAddress = "192.168.1.174";
    private final String deviceDisplayName = "Pixel 2";

    @Override
    protected String doInBackground(String... params) {
        pressKey(params[0]);
        return "";
    }


    private void pressKey(String keyCode) {
        try {
            InetAddress address = InetAddress.getByName(ipAddress);
            SamsungRemote remote = new SamsungRemote(address);
            TVReply reply = remote.authenticate(deviceDisplayName); // Argument is the device name (displayed on television).
            if (reply == TVReply.ALLOWED) {
                remote.keycode(keyCode);
            }
            remote.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
    protected void onPostExecute(String result){
        super.onPostExecute(result);
    }
}
