package com.jordansportfolio.samsungremote;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.jordansportfolio.samsungremote.samsungtvcontrol.SamsungRemote;
import com.jordansportfolio.samsungremote.samsungtvcontrol.TVReply;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> log;
    private final boolean debug = true;
    private final String ipAddress = "192.168.1.174";
    private final String deviceDisplayName = "Pixel 2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log = new ArrayList<String>();



        ImageView power = (ImageView) findViewById(R.id.btn_power);
        power.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Power Button 2 clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_POWER").get();
                    //pressKey("KEY_POWER");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView volumeUp = (ImageView) findViewById(R.id.btn_volume_up);
        volumeUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Volume Up clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_VOLUP").get();
                    //pressKey("KEY_VOLUP");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView volumeDown = (ImageView) findViewById(R.id.btn_volume_down);
        volumeDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Volume Down clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_VOLDOWN").get();
                    //pressKey("KEY_VOLDOWN");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView channelUp = (ImageView) findViewById(R.id.btn_channel_up);
        channelUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Channel Up clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_CHUP").get();
                    //pressKey("KEY_CHUP");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView channelDown = (ImageView) findViewById(R.id.btn_channel_down);
        channelDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Channel Down clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_CHDOWN").get();
                    //pressKey("KEY_CHDOWN");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        Button menu = (Button) findViewById(R.id.btn_menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Menu clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_MENU").get();
                    //pressKey("KEY_MENU");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        Button smartHub = (Button) findViewById(R.id.btn_smart_hub);
        smartHub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Smart Hub clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_APP_LIST").get();
                    //pressKey("KEY_APP_LIST");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView keyboard = (ImageView) findViewById(R.id.btn_keyboard);
        keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Keyboard clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("001").get();
                    //pressKey("001");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView up = (ImageView) findViewById(R.id.btn_up_nav);
        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Up clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_UP").get();
                    //pressKey("KEY_UP");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView down = (ImageView) findViewById(R.id.btn_down_nav);
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Down clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_DOWN").get();
                    //pressKey("KEY_DOWN");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView left = (ImageView) findViewById(R.id.btn_left_nav);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Left clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_LEFT").get();
                    //pressKey("KEY_LEFT");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView right = (ImageView) findViewById(R.id.btn_right_nav);
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Right clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_RIGHT").get();
                    //pressKey("KEY_RIGHT");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView enter = (ImageView) findViewById(R.id.btn_enter_nav);
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Enter clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_ENTER").get();
                    //pressKey("KEY_ENTER");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView back = (ImageView) findViewById(R.id.btn_return);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Back clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_RETURN").get();
                    //pressKey("KEY_RETURN");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView exit = (ImageView) findViewById(R.id.btn_exit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Exit clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_EXIT").get();
                    //pressKey("KEY_EXIT");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView rewind = (ImageView) findViewById(R.id.btn_fast_rewind);
        rewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Rewind clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_REWIND").get();
                    //pressKey("KEY_REWIND");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView play = (ImageView) findViewById(R.id.btn_play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Play clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_PLAY").get();
                    //pressKey("KEY_PLAY");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView pause = (ImageView) findViewById(R.id.btn_pause);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Pause clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_PAUSE").get();
                    //pressKey("KEY_PAUSE");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });

        ImageView fastForward = (ImageView) findViewById(R.id.btn_fast_forward);
        fastForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    addToLog("Fast Forward clicked");
                    RemoteAsync remoteAsync = new RemoteAsync();
                    String result = remoteAsync.execute("KEY_FF").get();
                    //pressKey("KEY_FF");
                } catch(Exception e) {
                    Log.e("Exception on helper", e.toString());
                }
            }
        });
    }

    private void pressKey(String keyCode) {
        try {
            InetAddress address = InetAddress.getByName(ipAddress);
            SamsungRemote remote = new SamsungRemote(address);
            TVReply reply = remote.authenticate(deviceDisplayName); // Argument is the device name (displayed on television).
            if (reply == TVReply.ALLOWED) {
                remote.keycode(keyCode);
            }
            remote.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private void addToLog(String message) {
        if(debug) {
            log.add(message);
        }
    }

    private ArrayList<String> getLog() {
        if(debug) {
            return log;
        }

        return null;
    }

    private String getLog(int index) {
        if(debug) {
            return log.get(index);
        }

        return null;
    }

    private void printLog() {
        if(debug) {
            for (int i = 0; i < log.size(); i++) {
                System.out.println(log.get(i));
            }
        }
    }
}
